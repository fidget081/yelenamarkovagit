/**
 * Примерные данные для заполнения, вы можете использовать свои данные.
 *  Имя: "Firefox",   Компания: "Mozilla",        Процент: "8.01%"
 *  Имя: "Chrome",    Компания: "Google",         Процент: "68.26%"
 *  Имя: "Edge",      Компания: "Microsoft",      Процент: "6.67%"
 *  Имя: "Opera",     Компания: "Opera Software", Процент: "1.31%"
 *
 */
const form = document.querySelector('#subForm');
const docForm = document.querySelector('#form');

const inputBrowser = document.querySelector("input[name=browser]");
// const browserValue = inputBrowser.value.trim();
const inputCompany = document.querySelector("input[name=company]");
// const companyValue = inputCompany.value.trim();
const inputPercent = document.querySelector("input[name=percent]");
// const percentValue = inputPercent.value.trim();


const objResult = [];
form.addEventListener('click', function (event) {
  event.preventDefault();

  const obj = {};

  const inputBrowser = document.querySelector("input[name=browser]");
  const browserValue = inputBrowser.value.trim();
  const inputCompany = document.querySelector("input[name=company]");
  const companyValue = inputCompany.value.trim();
  const inputPercent = document.querySelector("input[name=percent]");
  const percentValue = inputPercent.value.trim();

  if (!browserValue || !companyValue || !percentValue) {
    form.setAttribute('disabled', 'disabled');
    throw new Error('No data!');
  } else {
    form.removeAttribute('disabled');
  }

  obj.name = browserValue;
  obj.company = companyValue;
  obj.marketShare = percentValue;

  objResult.push(obj);

  objResult.sort(function (a, b) {
    return b.marketShare - a.marketShare;
  })

  docForm.reset();
  return objResult[0];
});

inputBrowser.addEventListener('input', () => {
  const browserValue = inputBrowser.value.trim();

  if (!isNaN(browserValue)) {
    inputBrowser.setAttribute('class', 'error');
    throw new Error('numbers are not allowed'); // почему не работает по каждому символу?
  } else {
    inputBrowser.setAttribute('class', '');
  }

  if (!browserValue) {
    inputBrowser.setAttribute('class', 'error');
    throw new Error('No data!');
  } else {
    inputBrowser.setAttribute('class', '');
  }

  if (browserValue.length < 3) {
    inputBrowser.setAttribute('class', 'error');
    throw new Error('No data length!');
  } else {
    inputBrowser.setAttribute('class', '');
  }
})


inputCompany.addEventListener('input', () => {
  const companyValue = inputCompany.value.trim();

  if (!isNaN(companyValue)) {
    inputCompany.setAttribute('class', 'error');
    throw new Error('numbers are not allowed');
  } else {
    inputCompany.setAttribute('class', '');
  }

  if (!companyValue) {
    inputCompany.setAttribute('class', 'error');
    throw new Error('No data!');
  } else {
    inputCompany.setAttribute('class', '');
  }

  if (companyValue.length < 3) {
    inputCompany.setAttribute('class', 'error');
    throw new Error('No data length!');
  } else {
    inputCompany.setAttribute('class', '');
  }
})


inputPercent.addEventListener('input', () => {
  const percentValue = inputPercent.value.trim();

  if (!percentValue) {
    inputPercent.setAttribute('class', 'error');
    throw new Error('No data!');
  } else {
    inputPercent.setAttribute('class', '');
  }

  if (percentValue < 0) {
    inputPercent.setAttribute('class', 'error');
    throw new Error('Number < 0!');
  } else {
    inputPercent.setAttribute('class', '');
  }

  if (isNaN(percentValue)) {
    inputPercent.setAttribute('class', 'error');
    throw new Error('string are not allowed'); // почему не работает по каждому символу?
  } else {
    inputPercent.setAttribute('class', '');
  }

  if (percentValue.length <= 1) {
    inputPercent.setAttribute('class', 'error');
    throw new Error('No data length!');
  } else {
    inputPercent.setAttribute('class', '');
  }
})


document.querySelector(".results button").addEventListener('click', function () {
  if (!objResult[0].name || !objResult[0].company || !objResult[0].marketShare) {
    throw new Error('Недостаточно данных!');
  }

  document.getElementById('result').innerHTML =
    `Самый востребованный браузер это ${objResult[0].name} от компании ${objResult[0].company} с процентом использования ${objResult[0].marketShare}%`;
})