import { dataArr } from './data.js';

export const dataSort = dataArr.sort(function (a, b) {
  return b.marketShare - a.marketShare;
})