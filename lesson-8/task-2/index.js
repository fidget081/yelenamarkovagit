/**
 * Доработать форму из 1-го задания.
 * 
 * Добавить обработчик сабмита формы.
 * 
 * Для того что бы увидеть результат откройте index.html файл в браузере.
 * 
 * Обязательно!
 * 1. При сабмите формы страница не должна перезагружаться
 * 2. Генерировать ошибку если пользователь пытается сабмитить форму с пустыми или содержащими только пробел(ы) полями.
 * 3. Если поля формы заполнены и пользователь нажимает кнопку Вход → вывести в консоль объект следующего вида
 * {
 *  email: 'эмейл который ввёл пользователь',
 *  password: 'пароль который ввёл пользователь',
 *  remember: 'true/false'
 * }
*/

// РЕШЕНИЕ

const form = document.getElementById('form');
const div = document.createElement('div');
const label = document.createElement('label');
const input = document.createElement('input');
const button = document.createElement('button');

div.classList.add('form-group');

label.setAttribute('for', 'email');
label.innerText = 'Электропочта';

input.setAttribute('type', 'email');
input.classList.add('form-control');
input.id = 'email';
input.setAttribute('placeholder', 'Введите свою электропочту');

button.setAttribute('type', 'submit');
button.classList.add('btn', 'btn-primary');


div.appendChild(label);
div.appendChild(input);
form.prepend(div);

//clone 1

const divPassword = div.cloneNode(true);
label.setAttribute('for', 'password');
label.innerText = 'Пароль';

input.setAttribute('type', 'password');
input.id = 'password';
input.setAttribute('placeholder', 'Введите пароль');


//clone 2

const divCheck = div.cloneNode(true);
div.classList.add('form-check');
input.setAttribute('type', 'checkbox');
input.classList.toggle('form-control');
input.classList.toggle('form-check-input');
input.id = 'exampleCheck1';
input.setAttribute('placeholder', '');

label.classList.add('form-check-label');
label.setAttribute('for', 'exampleCheck1');
label.innerText = 'Запомнить меня';

div.appendChild(input);
div.appendChild(label);
form.prepend(divCheck);
form.prepend(divPassword);

button.innerText = 'Вход';

form.appendChild(button);


const email = form.querySelector('#email');
const password = form.querySelector('#password');
const checkbox = form.querySelector('#exampleCheck1');

  
form.addEventListener('submit', function(event){
  event.preventDefault();
  
  const emailValue = email.value.trim();
  const passValue = password.value.trim();
  
  if(!emailValue || !passValue){
    throw new Error ('No data!');
  }

  // checkbox.checked
  const chb = !!checkbox.checked;

  const obj = {
    email: emailValue,
    password: passValue,
    remember: chb
  }

  console.log(obj);
});