/**
 * Создать форму динамически при помощи JavaScript.
 * 
 * В html находится пример формы которая должна быть сгенерирована.
 * 
 * Для того что бы увидеть результат откройте index.html файл в браузере.
 * 
 * Обязательно!
 * 1. Для генерации элементов обязательно использовать метод document.createElement
 * 2. Для установки атрибутов элементам обязательно необходимо использовать document.setAttribute
 * 3. Всем созданным элементам необходимо добавить классы как в разметке
 * 4. После того как динамическая разметка будет готова необходимо удалить код в HTML который находится между комментариями
*/

// РЕШЕНИЕ
const form = document.getElementById('form');
const div = document.createElement('div');
const label = document.createElement('label');
const input = document.createElement('input');
const button = document.createElement('button');

div.classList.add('form-group');

label.setAttribute('for', 'email');
label.innerText = 'Электропочта';

input.setAttribute('type', 'email');
input.classList.add('form-control');
input.id = 'email';
input.setAttribute('placeholder', 'Введите свою электропочту');

button.setAttribute('type', 'submit');
button.classList.add('btn', 'btn-primary');


div.appendChild(label);
div.appendChild(input);
form.prepend(div);

//clone 1
const divPassword = div.cloneNode(true);
label.setAttribute('for', 'password');
label.innerText = 'Пароль';

input.setAttribute('type', 'password');
input.id = 'password';
input.setAttribute('placeholder', 'Введите пароль');

form.prepend(divPassword);


//clone 2

const divCheck = div.cloneNode(true);
div.classList.add('form-check');
input.setAttribute('type', 'checkbox');
input.classList.toggle('form-control');
input.classList.toggle('form-check-input');
input.id = 'exampleCheck1';
input.setAttribute('placeholder', '');

label.classList.add('form-check-label');
label.setAttribute('for', 'exampleCheck1');
label.innerText = 'Запомнить меня';

div.appendChild(input);
div.appendChild(label);
form.prepend(divCheck);



button.innerText = 'Вход';
form.appendChild(button);

