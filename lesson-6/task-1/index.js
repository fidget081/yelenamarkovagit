/**
 * Задача 1.
 *
 * Создайте функцию `f`, которая возвращает куб числа, переданного в качестве аргумента.
 *
 * Условия:
 * - Генерировать ошибку, если в качестве аргумента был передан не числовой тип.
 */

// РЕШЕНИЕ


function f2(f){
  if (typeof f !== 'number'){
    throw new Error('Is not of type Number!');
  }
  if(isNaN(f)){
    throw new Error('Not a Number!');
  }

  return f**3;
}

console.log(f2(2)); // 8
