/**
 * Задача 2.
 *
 * Создайте функцию `f`, которая возвращает сумму всех переданных числовых аргументов.
 *
 * Условия:
 * - Функция должна принимать любое количество аргументов;
 * - Генерировать ошибку, если в качестве любого входного аргумента было предано не число.
 */

// РЕШЕНИЕ

function f(){
  let sum = 0;

  for (let i = 0; i < arguments.length; i++){
    if (typeof arguments[i] !== 'number'){
      throw new Error('Is not of type Number!');
    }

    if(isNaN(arguments[i])){
      throw new Error('Not a Number!');
    }
    sum += arguments[i];
  }
  return sum;
}

console.log(f(1, 1, 1, 2, 1, 1, 1, 1)); // 9
console.log(f(1, 1, 1, 2, 5, 1, 1, 5)); // 17

