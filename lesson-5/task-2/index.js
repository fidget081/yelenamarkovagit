/*
 * Задача 2.
 *
 * Создайте объект `user`, со свойствами `name`, `surname`, `job` и `data`.
 * 
 * При чтении свойства `data` должна возвращаться строка с текстом.
 * Возвращаемая строка должна содержать текст: `Привет! Я `name` `surname` и я работаю `job` `.
 * 
 * 
 * Значения свойств `name`, `surname`, `job` в объекте `user` нужно получать из функции prompt().
 * 
 * Условия:
 * - Свойство `data` обязательно должно быть геттером. 
 * 
 * Обратите внимание!
 * - Для того что бы обратиться к свойству оъекта необходимо использовать this.name, this.surname и this.job. * 
 */

// РЕШЕНИЕ


const name = prompt('Введите Имя:');
const surname = prompt('Введите Фамилию:');
const job = prompt('Введите свою профессию:');

const user = {
  name,
  surname,
  job,
  get data() {
      return `Привет! Я ${this.name} ${this.surname} и я работаю ${this.job}.`;
  },
};

console.log(user.data);