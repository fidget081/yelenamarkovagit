/**
 * Доработайте функцию что бы она возвращала объект из переданного вложенного массива
 * 
 * Фукнция принимает 1 аргумента
 * 1. Массив из массивов который содержит 2 элемента — [ [ element1, element2 ] ]
 * 
 * ЗАПРЕЩЕНО ИСПОЛЬЗОВАТЬ ВСТРОЕННЫЙ МЕТОД Object.fromEntries
 * 
 * Обратите внимание!
 * 1. Генерировать ошибку если второй элемент вложенного массива не число, не строка или не null
 * 2. Обязательно использовать деструктуризацию при извлечении элементов массива
 * 3. Если в качестве второго аргумента был передан массив вида [ [ element1, element2 ] ], то его так же нужно преобразовать в объект
 * 4. Для перебора массива можно воспользоваться циклом for..of.
*/

function fromEntries(entries){
  const obj = {};
    for(let key of entries){    
      obj[key[0]] = key[1];
      try {
        if(typeof key[1] !== 'number' || typeof key[1] !== 'string' || typeof key[1] !== null ) {
          throw new Error ('Error type!');
        }
      } catch {
        if(Array.isArray(key[1])){
          const obj1 = {};
            for(let key1 of key[1]){
              obj1[key1[0]] = key1[1];
            }

          obj[key[0]] = obj1;  
        }
      }
  }
  return obj;
};

console.log(fromEntries([['name', 'John'], ['age', 35]])); // { name: 'John', age: 35 }
console.log(fromEntries([['name', 'John'], ['address', [['city', 'New  York']]]])); // { name: 'John', address: { city: 'New  York' } }
